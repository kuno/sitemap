ALTER DATABASE sitemap SET timezone = 'utc';

CREATE TABLE sitemap (
    iri      text NOT NULL PRIMARY KEY,
    modified timestamptz DEFAULT NULL,
    fetched  timestamptz DEFAULT NULL,
    downloading BOOLEAN DEFAULT FALSE
);

CREATE INDEX ON sitemap (modified);
CREATE INDEX ON sitemap (fetched);
CREATE INDEX ON sitemap (downloading);

CREATE TABLE page (
    iri      text NOT NULL PRIMARY KEY,
    modified timestamptz DEFAULT NULL,
    fetched  timestamptz DEFAULT NULL,
    downloading BOOLEAN DEFAULT FALSE
);

CREATE INDEX ON page (modified);
CREATE INDEX ON page (fetched);
CREATE INDEX ON page (downloading);
