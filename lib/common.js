/** -*- mode: js; -*-
 *
 * This file is part of sitemap.
 * Copyright 2016  Kuno Woudt <kuno@frob.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txtt.
 */

'use strict';

const pgPromise = require ('pg-promise');

function promiseMap (list) {
    const acc = [];

    return list.reduce ((memo, fn) => {
        return memo.then (fn).then (result => {
            acc.push (result);
            return acc;
        });
    }, Promise.resolve ());
}

class Database {

    constructor () {
        const pgOptions = {
            capSQL: true
        };

        // FIXME: make configurable
        const pgConnection = {
            database: 'sitemap',
            user: 'sitemap',
            password: 'sitemap'
        };

        this.pgp = pgPromise (pgOptions);
        this.connection = this.pgp (pgConnection);
    }

    upsert (insert, update, params) {
        const errMsgs = [];

        return this.connection
                   .none (insert, params)
                   .then (() => { return 'inserted'; })
                   .catch (error => {
                       errMsgs.push ('INSERT ERROR. ' + error.toString ());
                       errMsgs.push ('INSERT ERROR. ' + error.detail);

                       return this.connection
                                  .none (update, params)
                                  .then (() => { return 'updated'; })
                                  .catch (error => {
                                      errMsgs.push ('UPDATE ERROR. ' + error.toString ());
                                      errMsgs.push ('UPDATE ERROR. ' + error.detail);
                                      console.log (errMsgs.join ('\n'));
                                  });
                   });
    }

    done () {
        this.pgp.end ();
    }
}

module.exports = {
    Database,
    promiseMap,
}
