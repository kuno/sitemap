
sitemap
=======

The goal of this project is to continuously fetch the musicbrainz sitemap and keep
track of all MB urls and their last modified date.

    sudo dnf install postgresql-server postgresql-contrib
    sudo postgresql-setup --initdb --unit postgresql
    sudo systemctl enable postgresql
    sudo systemctl start postgresql
    sudo su - postgres
    psql
    CREATE USER root WITH PASSWORD 'rootpw' CREATEUSER;
    CREATE USER sitemap WITH PASSWORD 'sitemap';
    CREATE DATABASE sitemap OWNER sitemap;
    \q
    exit
    touch ~/.pgpass
    chmod go-rw ~/.pgpass
    echo 'localhost:*:*:root:rootpw' >> ~/.pgpass
    echo 'localhost:*:*:sitemap:sitemap' >> ~/.pgpass
    sudo vim /var/lib/pgsql/data/pg_hba.conf

    # Make sure md5 auth is enabled, e.g.:
    sudo cat /var/lib/pgsql/data/pg_hba.conf | egrep -v '^#' | egrep -v '^$'

    local   all             all                                     md5
    host    all             all             127.0.0.1/32            md5
    host    all             all             ::1/128                 md5

    # Restart postgresql after making changes to pg_hba.conf
    sudo systemctl restart postgresql
    # Test the connection
    psql -U sitemap

    # Create the tables
    \i db.sql


License
=======

Copyright 2016  Kuno Woudt <kuno@frob.nl>

This program is free software: you can redistribute it and/or modify
it under the terms of copyleft-next 0.3.1.  See
[copyleft-next-0.3.1.txt](copyleft-next-0.3.1.txt).

